<?php 
// $target_path = "archivos/";
// $target_path = $target_path . basename($_FILES['uploadedfile']['name']);
// if (move_uploaded_file($_FILES['uploadedfile']['tmp_name'],$target_path)) {
//     echo "El archivo " . basename($_FILES['uploadedfile']['name']). " ha sido subido";
// }else{
//     echo "Hubo un error, intente de nuevo!";
// }

// Recibir datos de img
$nombre_img = $_FILES['img']['name'];
$tipo       = $_FILES['img']['type'];
$size       = $_FILES['img']['size'];

//Si existe imagen y tiene un tamaño correcto
if (($nombre_img == !NULL) && ($_FILES['imagen']['size'] <= 200000))
{
   //indicamos los formatos que permitimos subir a nuestro servidor
   if (($_FILES["imagen"]["type"] == "image/gif")
   || ($_FILES["imagen"]["type"] == "image/jpeg")
   || ($_FILES["imagen"]["type"] == "image/jpg")
   || ($_FILES["imagen"]["type"] == "image/png"))
   {
      // Ruta donde se guardarán las imágenes que subamos
      $directorio = $_SERVER['DOCUMENT_ROOT'].'/archivos/';
      // Muevo la imagen desde el directorio temporal a nuestra ruta indicada anteriormente
      move_uploaded_file($_FILES['imagen']['tmp_name'],$directorio.$nombre_img);
    }
    else
    {
       //si no cumple con el formato
       echo "No se puede subir una imagen con ese formato ";
    }
}
else
{
   //si existe la variable pero se pasa del tamaño permitido
   if($nombre_img == !NULL) echo "La imagen es demasiado grande ";
}
?>