export const PI = Math.PI;

export let user = "Oscar", pass = "Dolph"

// EXPORTACIÓN Y EJECUCIÓN POR DEFECTO SIN EXCEPCIONES(CLASES, FUNCIONES)
// export default function saludar(){
//     console.log(`Hola módulos +ES6`);
// }

// export default let thing = "Algo" NO PERMITIDO! con algo expresado pero si con algo declarado
// let declara = "Oscar";
// export default declara;

export default class Vuela{
    constructor(alas="Grandes"){
        this.alas = alas
        console.log(this.alas+' Hola CLASE');
    }
}