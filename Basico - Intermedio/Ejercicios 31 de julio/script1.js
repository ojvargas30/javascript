
console.log('Ejercicio 1');
console.log("Hola Mundo!");

document.write('<b>Ejercicio 1</b><p></p>');
document.write('Soy el primer script');

console.log('---------------------------');

console.log('Ejercicio 2');
let let1 = "vvvv";
alert(let1);

console.log('---------------------------');

console.log('Ejercicio 3');
const meses = ['Enero','Febrero', 'Marzo', 'Abril' , 'Mayo' , 'Junio',
               'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
console.log(meses);
meses.forEach(function(index,valor){
    console.log(`<li id="${index}">${valor+1}</li>`);
});

console.log('---------------------------');

console.log('Ejercicio 4');
const valores = [true, 5, false, "hola", "adios", 2];
if(valores[3].length > valores[4].length){
    console.log('El arreglo valores en posición 3 es mayor que él mismo en posición 4 = '+ valores[2]);
}else{
    console.log('El arreglo valores en posición 4 es mayor que él mismo en posición 3 = '+ valores[0]);
}

if(valores[3].length < valores[4].length){
    console.log('El arreglo valores en posición 3 es menor que él mismo en posición 4 = '+ valores[2]);
}else{
    console.log('El arreglo valores en posición 4 es menor que él mismo en posición 3 = '+ valores[0]);
}

console.log('---------------------------');

let sum = valores[1] + valores[5];
console.log('Resultado de la suma = ' + sum);
let resta = valores[1] - valores[5];
console.log('Resultado de la resta = ' +resta); 
let division = valores[1] / valores[5];
console.log('Resultado de la división = ' +division); 
let multiplication = valores[1] * valores[5];
console.log('Resultado de la multiplicación = ' +multiplication); 
let modulo = valores[1] % valores[5];
console.log('Resultado del modulo = ' +modulo); 

console.log('---------------------------');
console.log('Ejercicio 5');

let numero1 = 5;
let numero2 = 8;
 
if(numero1 < numero2) {
  console.log("numero1 no es mayor que numero2");
}
if(numero2 > 0) {
  console.log("numero2 es positivo");
}
if(numero1 < 0 || numero1 != 0) {
  console.log("numero1 es negativo o distinto de cero");
}
if(numero1+1<=numero2) {
  console.log("Incrementar en 1 unidad el valor de numero1 no lo hace mayor o igual que numero2");
}

console.log('---------------------------');

// funciones toUperCase-toLowerCase
let mensaje1 = "HolA";
let mensaje2 = mensaje1.toLowerCase(); // mensaje2 = "hola"
let bonus = mensaje1.toUpperCase();
// charAt

let mensaje3 = "Hola";
let letra = mensaje3.charAt(0); // letra = H
let letra2 = mensaje3.charAt(2);     // letra = l

//indexof(caracter)

let mensaje4 = "Hola";
let posicion = mensaje4.indexOf('a'); // posicion = 3
let posicion2 = mensaje4.indexOf('b');     // posicion = -1

// lastIndexOf()  // busca desde el final una posicion de caracter que ubica desde el prinicipio de la palabra.

// subString(inicio, final)

let mensaje5 = "Hola Mundo";
let porcion = mensaje5.substring(1, 8); // porcion = "ola Mun"
let porcion2 = mensaje5.substring(3, 4);     // porcion = "a"

// split(separador)

let mensaje6 = "Hola Mundo, soy una cadena de texto!";
let palabras = mensaje6.split(" ");
// palabras = ["Hola", "Mundo,", "soy", "una", "cadena", "de", "texto!"];

let palabra = "Hola";
let letras = palabra.split(""); // letras = ["H", "o", "l", "a"]

// document.write(prompt('Hola mundo'));

let bool = true;
let str = bool.toString();
console.log(typeof str);

let txt2 = "Hoal";
console.log(parseInt(txt2));

let num11 = 3;
console.log(isNaN(num11));
console.log(parseFloat(num11)+2134.123);

const v1 = 5;
const v2 = 1;

let res = v1 + v2;

console.log(res);

let v3 = new Date(2020, 07, 06);
let v4 = v3;

v3.setFullYear()

let v5 = 0 - 56;
console.log(v5);

let v6 = "Hola 'mundo'"
console.log(v6);

let v7 = 'Hola "mundo"'
console.log(v7);

let v8 = "Hola 'mundo', \n esta \t es \"";

let v9="YEah " + "V iA  je";
// let v10 = ["hola", "yeah", "2" , true, 1];
let v10 = ["hola", "yeah", "2"];

console.log(v9.toUpperCase());
console.log(v9.toLowerCase());
console.log(v9.charAt(2));
console.log(v9.concat());
console.log(v9.indexOf('h'));
console.log(v9.lastIndexOf('a',2));
console.log(v9.match(/a/gi));
console.log(v9.substr(2));
console.log(v9.substring(2,5));
// console.log(v9.split(""));
// console.log(v9.join(" "));
console.log(v10.pop());
console.log(v10);
console.log(v10.shift("yeah"));
console.log(v10);
console.log(v10.unshift("Number One"));
console.log(v10);
