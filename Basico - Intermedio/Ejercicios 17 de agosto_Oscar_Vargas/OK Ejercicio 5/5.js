// Completar las condiciones de los if del siguiente script para que los mensajes se
// muestren siempre de forma correcta:
var numero1 = 5;
var numero2 = 8;
if (numero1 < numero2) {
    console.log("numero1 no es mayor que numero2");
}
if (numero2 > 0) {
    console.log("numero2 es positivo");
}
if (numero1 < 0) {
    console.log("numero1 es negativo o distinto de cero");
}
if (numero++ < numero2) {
    console.log("Incrementar en 1 unidad el valor de numero1 no lo hace mayor o igual que numero2");
}
// funciones toUperCase-toLowerCase
var mensaje1 = "HolA";
var mensaje2 = mensaje1.toLowerCase(); // mensaje2 = "hola"
// charAt
var mensaje = "Hola";
var letra = mensaje.charAt(0); // letra = H
letra = mensaje.charAt(2); // letra = l
//indexof(caracter)
var mensaje = "Hola";
var posicion = mensaje.indexOf('a'); // posicion = 3
posicion = mensaje.indexOf('b'); // posicion = -1
// lastIndexOf() // busca desde el final una posicion de caracter que ubica desde el prinicipio de la palabra.
// subString(inicio, final)
var mensaje = "Hola Mundo";
var porcion = mensaje.substring(1, 8); // porcion = "ola Mun"
porcion = mensaje.substring(3, 4); // porcion = "a"
// split(separador)
var mensaje = "Hola Mundo, soy una cadena de texto!";
var palabras = mensaje.split(" ");
// palabras = ["Hola", "Mundo,", "soy", "una", "cadena", "de", "texto!"];
var palabra = "Hola";
var letras = palabra.split(""); // letras = ["H", "o", "l", "