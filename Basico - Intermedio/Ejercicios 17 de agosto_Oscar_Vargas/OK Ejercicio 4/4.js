
var valores = [true, 5, false,
    "hola", "adios", 2];

/*¿Determinar cuál de los dos elementos de texto es mayor?
 Utilizando exclusivamente los dos valores booleanos del array, determinar los
operadores necesarios para obtener un resultado true y otro resultado false */
if (valores[3].length < valores[4].length)
    console.log(`El arreglo en posición 3 hola no es mayor que el arreglo valores en la posición 4 devuelve: ` + valores[2]);
else
    console.log(`hola tiene una longitud mayor devuelve: ` + valores[5]);

/*Determinar el resultado de las cinco operaciones matemáticas realizadas con los
dos elementos numéricos. Revisión de ejercicios de saberes previos:*/
function cal(n1, n2, operacion) {
    switch (operacion) {
        case "s":
            console.log(`El resultado de la suma es: ` + (n1 + n2));
            break;
        case "r":
            console.log(`El resultado de la resta es: ` + (n1 - n2));
            break;
        case "m":
            console.log(`El resultado de la multiplicación es: ` + (n1 * n2));
            break;
        case "d":
            console.log(`El resultado de la división es: ` + (n1 / n2));
            break;
        case "mod":
            console.log(`El resultado del modulo es: ` + (n1 % n2));
            break;
        default:
            console.log(`El resultado de la suma es: ` + (n1 + n2));
            break;
    }
}

let n1 = valores[1];
let n2 = valores[5];
cal(n1, n2, "s");
cal(n1, n2, "r");
cal(n1, n2, "m");
cal(n1, n2, "d");
cal(n1, n2, "mod");
