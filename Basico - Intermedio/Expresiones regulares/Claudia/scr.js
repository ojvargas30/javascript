function validar() {
  var nombre, apellido, correo, usuario, clave, telefono, expresion;
  nombre = document.getElementsByTagName('nombre').value;
  Apellido = document.getElementsByTagName('apellido').value;
  correo = document.getElementsByTagName('correo').value;
  usuario = document.getElementsByTagName('usuario').value;
  clave = document.getElementsByTagName('clave').value;
  telefono = document.getElementsByTagName('telefono').value;

  expresion = /\w+@+\w\.+[a-z]/;


  if (nombre === "" || apellido === "" || correo === "" || usuario === "" || clave === "" || telefono === "") {
    alerta("Todos los campos son obligatorios")
    return false;
  }
  else if (nombre.length > 30) {
    alerta("El nombre es muy largo")
    return false;
  }
  else if (apellido.length > 80) {
    alerta("El apellido es muy largo")
    return false;
  }

  else if (correo.length > 100) {
    alerta("El correo es muy largo")
    return false;
  }

  else if (expresion.test(correo)) {
    alerta("el correo no es valido")
    return false;
  }
  else if (usuario.length > 20 || clave.length > 20) {
    alerta("El usuario y la clave deben contener 20 caracteres")
    return false;
  }

  else if (telefono.length > 10) {
    alerta("El telefono contiene 10 digitos")
    return false;
  }

  else if (isNaN(telefono)) {
    alerta("El numero de telefono ingresado no es numero");
    return false;
  }


}